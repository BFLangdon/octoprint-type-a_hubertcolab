var gcode_layers = [];
var bbbox = null;
var Model = function (){
  // GCode descriptions come from:
  //    http://reprap.org/wiki/G-code
  //    http://en.wikipedia.org/wiki/G-code
  //    SprintRun source code
	
  var lastLine = {x:0, y:0, z:0, e:0, f:0, extruding:false};
 
 	gcode_layers = [];
 	var layer = undefined;
 	bbbox = { min: { x:100000,y:100000,z:100000 }, max: { x:-100000,y:-100000,z:-100000 } };
    
    this.extrude = true;
    this.snap = false;
    this.scale = 1;
    this.world_topz = 0;
    this.largest_dim = 0;
 	
 	function newLayer(line) {
 		layer = { type: {}, layer: gcode_layers.count(), z: line.z, };
 		gcode_layers.push(layer);
 	}
 	function getLineGroup(line) {
 		if (layer == undefined)
 			newLayer(line);
 		var speed = Math.round(line.e / 1000);
 		var grouptype = (line.extruding ? 10000 : 0) + speed;
        var color = new THREE.Color(line.extruding ? document.getElementById("settings-modelColor1").value : document.getElementById("settings-modelColor2").value);
        var highlight = line.extruding ? 0x00ff00 : 0xff0000;
 		if (layer.type[grouptype] == undefined) {
 			layer.type[grouptype] = {
 				type: grouptype,
 				feed: line.e,
 				extruding: line.extruding,
 				color: color,
                highlight: highlight,
 				segmentCount: 0,
 				material: new THREE.LineBasicMaterial({
                      transparent: true,
					  opacity:line.extruding ? 0.3 : 0.2,
                      //depthWrite: false,
                      blending: THREE.NormalBlending,
					  linewidth: 1,
					  vertexColors: THREE.VertexColors }),
				geometry: new THREE.Geometry(),
			}
		}
		return layer.type[grouptype];
 	}
 	function addSegment(p1, p2) {
		var group = getLineGroup(p2);
		var geometry = group.geometry;
		
		group.segmentCount++;
        
        geometry.vertices.push(new THREE.Vertex(
            new THREE.Vector3(p1.x, p1.y, p1.z)));
        geometry.vertices.push(new THREE.Vertex(
            new THREE.Vector3(p2.x, p2.y, p2.z)));
        geometry.colors.push(group.color);
        geometry.colors.push(group.color);
        geometry.buffersNeedUpdate = true;
        geometry.verticesNeedUpdate = true;
        if (p2.e && !p2.f) {
			bbbox.min.x = Math.min(bbbox.min.x, p2.x);
			bbbox.min.y = Math.min(bbbox.min.y, p2.y);
			bbbox.min.z = Math.min(bbbox.min.z, p2.z);
			bbbox.max.x = Math.max(bbbox.max.x, p2.x);
			bbbox.max.y = Math.max(bbbox.max.y, p2.y);
			bbbox.max.z = Math.max(bbbox.max.z, p2.z);
		}
 	}
  	var relative = false;
	function delta(v1, v2) {
		return relative ? v2 : v2 - v1;
	}
	function absolute (v1, v2) {
		return relative ? v1 + v2 : v2;
	}

  this.parser = new GCodeParser({
    G0: function(args, line) {

      var newLine = {
        x: args.x !== undefined ? absolute(lastLine.x, args.x) : lastLine.x,
        y: args.y !== undefined ? absolute(lastLine.y, args.y) : lastLine.y,
        z: args.z !== undefined ? absolute(lastLine.z, args.z) : lastLine.z,
        e: args.e !== undefined ? absolute(lastLine.e, args.e) : lastLine.e,
        f: args.f !== undefined ? absolute(lastLine.f, args.f) : 0,
      };

    if (delta(lastLine.e, newLine.e) > 0) {
      newLine.extruding = delta(lastLine.e, newLine.e) > 0;
      if (layer == undefined || newLine.z != layer.z)
        newLayer(newLine);
    }
    addSegment(lastLine, newLine);
      lastLine = newLine;
    },
    G1: function(args, line) {
      // Example: G1 Z1.0 F3000
      //          G1 X99.9948 Y80.0611 Z15.0 F1500.0 E981.64869
      //          G1 E104.25841 F1800.0
      // Go in a straight line from the current (X, Y) point
      // to the point (90.6, 13.8), extruding material as the move
      // happens from the current extruded length to a length of
      // 22.4 mm.

      var newLine = {
        x: args.x !== undefined ? absolute(lastLine.x, args.x) : lastLine.x,
        y: args.y !== undefined ? absolute(lastLine.y, args.y) : lastLine.y,
        z: args.z !== undefined ? absolute(lastLine.z, args.z) : lastLine.z,
        e: args.e !== undefined ? absolute(lastLine.e, args.e) : lastLine.e,
        //e: args.e !== undefined ? absolute(lastLine.e, args.e) : 0,
        //f: args.f !== undefined ? absolute(lastLine.f, args.f) : lastLine.f,
        f: args.f !== undefined ? absolute(lastLine.f, args.f) : 0,
      };
      /* layer change detection is or made by watching Z, it's made by
         watching when we extrude at a new Z position */
		if (delta(lastLine.e, newLine.e) > 0) {
			newLine.extruding = delta(lastLine.e, newLine.e) > 0;
			if (layer == undefined || newLine.z != layer.z)
				newLayer(newLine);
		}
		addSegment(lastLine, newLine);
      lastLine = newLine;
    },

    G21: function(args) {
      // G21: Set Units to Millimeters
      // Example: G21
      // Units from now on are in millimeters. (This is the RepRap default.)

      // No-op: So long as G20 is not supported.
    },

    G90: function(args) {
      // G90: Set to Absolute Positioning
      // Example: G90
      // All coordinates from now on are absolute relative to the
      // origin of the machine. (This is the RepRap default.)

      relative = false;
    },

    G91: function(args) {
      // G91: Set to Relative Positioning
      // Example: G91
      // All coordinates from now on are relative to the last position.

      // TODO!
      relative = true;
    },

    G92: function(args) { // E0
      // G92: Set Position
      // Example: G92 E0
      // Allows programming of absolute zero point, by reseting the
      // current position to the values specified. This would set the
      // machine's X coordinate to 10, and the extrude coordinate to 90.
      // No physical motion will occur.

      // TODO: Only support E0
      var newLine = lastLine;
      newLine.x= args.x !== undefined ? args.x : newLine.x;
      newLine.y= args.y !== undefined ? args.y : newLine.y;
      newLine.z= args.z !== undefined ? args.z : newLine.z;
      newLine.e= args.e !== undefined ? args.e : newLine.e;
      lastLine = newLine;
    },

    M82: function(args) {
      // M82: Set E codes absolute (default)
      // Descriped in Sprintrun source code.

      // No-op, so long as M83 is not supported.
    },

    M84: function(args) {
      // M84: Stop idle hold
      // Example: M84
      // Stop the idle hold on all axis and extruder. In some cases the
      // idle hold causes annoying noises, which can be stopped by
      // disabling the hold. Be aware that by disabling idle hold during
      // printing, you will get quality issues. This is recommended only
      // in between or after printjobs.

      // No-op
    },

    'default': function(args, info) {
      //console.error('Unknown command:', args.cmd, args, info);
    },
  });

}

Model.prototype.createObjectFromGCode = function () {
    //console.log("Layer Count ", gcode_layers.count());
    
    this.object = new THREE.Object3D();
    
    for (var lid in gcode_layers) {
        var layer = gcode_layers[lid];
        for (var tid in layer.type) {
            var type = layer.type[tid];
            this.object.add(new THREE.Line(type.geometry, type.material, THREE.LinePieces));
        }
    }

    // Auto Sizing
    var bxdif = bbbox.max.x - bbbox.min.x;
    var bydif = bbbox.max.y - bbbox.min.y;
    var bzdif = bbbox.max.z - bbbox.min.z;
    
    this.largest_dim = Math.max(bxdif,bydif,bzdif);
    
    this.scale = 800 / this.largest_dim;
    
    // Center
    var center = new THREE.Vector3(
                                   bbbox.min.x + (bxdif / 2),
                                   bbbox.min.y + (bydif / 2),
                                   bbbox.min.z + (bzdif / 2));
    
    this.object.position = center.multiplyScalar(-this.scale);
    
    this.object.scale.multiplyScalar(this.scale);
    
    this.world_topz = this.largest_dim * this.scale + this.object.position.z;
    
    if(!this.extrude)
        this.showOnlyExtrusion();
    
    return this.object;
}

Model.prototype.createObjectFromGCodeLayer = function (layer_num) {
    this.layer_object = new THREE.Object3D();
    
    var layer = gcode_layers[layer_num];
    for(var tid in layer.type) {
        var type = layer.type[tid];
        this.layer_object.add(new THREE.Line(type.geometry, type.material, THREE.LinePieces));
    }
    
    var z_offset = layer.z;
    
    // Auto Sizing
    var bxdif = bbbox.max.x - bbbox.min.x;
    var bydif = bbbox.max.y - bbbox.min.y;
    var bzdif = bbbox.max.z - bbbox.min.z;
    
    var center = new THREE.Vector3(
                                   bbbox.min.x + (bxdif / 2),
                                   bbbox.min.y + (bydif / 2),
                                   z_offset);
    
    this.layer_object.position = center.multiplyScalar(-this.scale);
    
    this.layer_object.scale.multiplyScalar(this.scale);
    
    return this.layer_object;
}

Model.prototype.parse_code = function (gcode) {
    this.parser.parse(gcode);
}

Model.prototype.showOnlyExtrusion = function () {
    var line_array = this.object.getDescendants();
    for(var cid in line_array) {
        var child = line_array[cid];
        if(child) {
            if(child.material.opacity == 0.1 || child.material.opacity == 0.4 || child.material.opacity == 0.9) {
                child.material.visible = false;
            } else {
                //if(view_mode_3d)
                    //child.material.opacity = 0.1;
            }
        }
    }
}

Model.prototype.showEverything = function () {
    var line_array = this.object.getDescendants();
    for(var cid in line_array) {
        var child = line_array[cid];
        if(child) {
            if(child.material.opacity == 0.1 || child.material.opacity == 0.4 || child.material.opacity == 0.9) {
                child.material.visible = true;
            } else {
                //if(view_mode_3d)
                    //child.material.opacity = 0.6;
            }
        }
    }
}
