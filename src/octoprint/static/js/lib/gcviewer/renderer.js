var view_mode_3d = true;

function createScene(element) {

  // Renderer
  var renderer = new THREE.WebGLRenderer({alpha: true, clearColor:0xffffff, clearAlpha: 0});
  renderer.setSize(element.width(), element.height());
  element.append(renderer.domElement);
  renderer.setClearColor( 0xffffff, 0);
  renderer.clear();

  renderer.sortObjects = true;

  // Scene
  var scene0 = new THREE.Scene();
    var bgscene = new THREE.Scene();
  
  // start off with 3d view
  view_mode_3d = true;

  // Lights...
  /*[[0,0,1,  0xFFFFCC], //1111 1111 1111 1111 1100 1100
   [0,1,0,  0xFFCCFF],
   [1,0,0,  0xCCFFFF],
   [0,0,-1, 0xCCCCFF],
   [0,-1,0, 0xCCFFCC],
   [-1,0,0, 0xFFCCCC]].forEach(function(position) {
    var light = new THREE.DirectionalLight(position[3]);
    light.position.set(position[0], position[1], position[2]).normalize();
    scene0.add(light);
  });*/

  // Camera...
  var fov    = 45,
      aspect = element.width() / element.height(),
      near   = 1,
      far    = 100000;
    var camera3d = new THREE.PerspectiveCamera(fov, aspect, near, far);
  //camera.rotationAutoUpdate = true;
    camera3d.position.x = 0;
    camera3d.position.y = -500;
    camera3d.position.z = 1000;
    camera3d.lookAt(scene0.position);
    
    var top_camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
    top_camera.position.x = 0;
    top_camera.position.y = 0;
    top_camera.position.z = 1000;
    top_camera.lookAt(scene0.position);

    scene0.add(camera3d);
    scene0.add(top_camera);
  
  var element_id = document.getElementById('renderArea');
  controls = new THREE.TrackballControls(camera3d,element_id);
    controls.noRotate = true;
    controls.noZoom = true;
    controls.noPan = true;
  controls.dynamicDampingFactor = 0.15;
  controls.rotateSpeed = 2; //sen-sitivity for rotation
  //controls.staticMoving = true; //how camera rotates
    
    top_controls = new THREE.TrackballControls(top_camera,element_id);
    top_controls.dynamicDampingFactor = 0.15;
    top_controls.noRotate = true;
    top_controls.noZoom = true;
    top_controls.noPan = true;
    
    // Background
    bgTex = THREE.ImageUtils.loadTexture( 'static/img/buildplate.png' );
    bgTex.flipY = false;
    var bgback = new THREE.Mesh(new THREE.PlaneGeometry(99999, 99999),
                            new THREE.MeshBasicMaterial({side: THREE.DoubleSide})
                            );
    bgback.material.depthTest = false;
    bgback.material.depthWrite = false;
    bgback.rotation.x = Math.PI/2;
    
    var bgside = new THREE.Mesh(new THREE.PlaneGeometry(99999, 99999),
                            new THREE.MeshBasicMaterial({side: THREE.DoubleSide})
                            );
    bgside.material.depthTest = false;
    bgside.material.depthWrite = false;
    bgside.rotation.x = Math.PI/2;
    bgside.rotation.y = Math.PI/2;
    
    var bgfloor = new THREE.Mesh(new THREE.PlaneGeometry(99999, 99999),
                                new THREE.MeshBasicMaterial({side: THREE.DoubleSide})
                                );
    bgfloor.material.depthTest = false;
    bgfloor.material.depthWrite = false;
    
    bgscene.add(bgback);
    bgscene.add(bgside);
    bgscene.add(bgfloor);
    
    renderer.autoClear = false;
    
  // Action!
  function render() {
      renderer.clear();
      if(view_mode_3d) {
          controls.update();
          renderer.render(bgscene, camera3d);
          renderer.render(scene0, camera3d);
      } else {
          top_controls.update();
          renderer.render(bgscene, top_camera);
          renderer.render(scene0, top_camera);
      }
      requestAnimationFrame(render); // And repeat...
  }
  render();

  // Fix coordinates up if window is resized.
  $(window).on('resize', function() {
    renderer.setSize(element.width(), element.height());
    camera3d.aspect = element.width() / element.height();
    camera3d.updateProjectionMatrix();
    top_camera.aspect = element.width() / element.height();
    top_camera.updateProjectionMatrix();
    controls.screen.width = window.innerWidth;
    controls.screen.height = window.innerHeight;
    top_controls.screen.width = window.innerWidth;
    top_controls.screen.height = window.innerHeight;
  });

  return scene0;
}

/*function updateCam(camera) {
    camera.position.x = 0;
    camera.position.y = -500;
    camera.position.z = 1000;
    camera.lookAt(pos);
}*/
